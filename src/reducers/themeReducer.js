import {types} from "../types/types";

const initialState = {
    theme : 'light'
}


export const themeReducer = (state = initialState, action) => {

    switch (action.type) {
        case types.themeUpdated:
            return {
                ...state,
                theme: action.payload
            }
        default:
            return state;
    }
}