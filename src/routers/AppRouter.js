import React, {useEffect, useState} from 'react';
import {BrowserRouter as Router, Switch, Redirect} from "react-router-dom";

import {AuthRouter} from "./AuthRouter";
import {useDispatch} from "react-redux";

import {JournalScreen} from "../components/journal/JournalScreen";
import {LoadingTwo} from "../components/loading/LoadingTwo";

import {startLoadingNotes} from "../actions/notes";
import {login} from "../actions/auth";

import {PublicRoute} from "./PublicRoute";
import {PrivateRoute} from "./PrivateRoute";

import {getAuth, onAuthStateChanged} from 'firebase/auth';
/*
    *Different Loader

import {Loading} from "../components/loading/Loading";
import {LoadingThree} from "../components/loading/LoadingThree";
import {LoadingFour} from "../components/loading/LoadingFour";
import {LoadingFive} from "../components/loading/LoadingFive";
    *
*/

export const AppRouter = () => {

    const dispatch = useDispatch();
    const [checking, setChecking] = useState(true);
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {

        onAuthStateChanged(getAuth(), async (user) => {

            if (user?.uid) {

                let name =  user.displayName ||
                            user.reloadUserInfo.providerUserInfo[0].displayName ||
                            user.reloadUserInfo.screenName || 'User' ;

                dispatch(login(user.uid, name, user.photoURL));

                setIsLoggedIn(true);

                dispatch(startLoadingNotes(user.uid));

            } else {
                setIsLoggedIn(false);
            }

            setChecking(false);

        });


    }, [dispatch, setChecking, setIsLoggedIn]);

    if (checking) {
        return (
            // <Loading/>
            <LoadingTwo/>
            // <LoadingThree />
            // <LoadingFour />
            // <LoadingFive />
        )
    }


    return (

        <Router>
            <div>
                <Switch>

                    <PublicRoute isAuthenticated={isLoggedIn} path="/auth" component={AuthRouter}/>
                    <PrivateRoute isAuthenticated={isLoggedIn} exact path="/" component={JournalScreen}/>

                    <Redirect to="/auth/login"/>

                </Switch>
            </div>
        </Router>

    )
};