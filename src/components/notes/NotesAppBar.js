import {useState} from 'react';

import moment from 'moment';
import 'moment/locale/es';

import {useDispatch, useSelector} from "react-redux";
import {startSaveNote, startUploading} from "../../actions/notes";

export const NotesAppBar = () => {

    const {date} = useSelector(state=>state.notes.active);
    const noteDate = moment(date);

    const dispatch = useDispatch();
    const {active} = useSelector(state=> state.notes);

    const {theme} = useSelector(state => state.theme);

    const [closeModal, setCloseModal] = useState(true);

    const handleSave = ()=>{
        console.log(active)
        dispatch(startSaveNote(active));
    }

    const handlePicture = ()=>{
        console.log('click en picture');
        document.querySelector('#imagenUpload').click(); // to simulate event click
    }

    const changePicture = (e)=>{
        const file = e.target.files[0];
        console.log(file)
        dispatch( startUploading(file));
    }

    const handleContract = () => {

        let menuDiv = document.querySelector('#sidebar-component');

        if (closeModal) {
            menuDiv.classList.add('sidebar-close');
        } else {
            if (theme === 'light') {
                menuDiv.classList.add('journal__sidebar-light');
            } else {
                menuDiv.classList.add('journal__sidebar-dark');
            }
            menuDiv.classList.remove('sidebar-close');
        }

        setCloseModal(!closeModal);
    }

    return (

        // <div className="notes__appbar">
        <div className={`${theme === 'light' ? "notes__appbar-light": "notes__appbar-dark"}`}>

            <span onClick={handleContract}>
                <i className="fas fa-bars"/>
            </span>

            <span>{noteDate.format("dddd, D MMMM YYYY, h:mm A")}</span>

            <div>

                <button className="btn" onClick={handlePicture}>
                    Picture
                </button>

                <input type="file" name="imagenUpload" id="imagenUpload"
                       style={{display:'none'}} onChange={changePicture}/>

                <button className="btn btn-success" onClick={handleSave}>
                    Save
                </button>

            </div>

        </div>

    )
};