import React, {useEffect, useRef} from 'react';
import {NotesAppBar} from "./NotesAppBar";
import {useDispatch, useSelector} from "react-redux";
import {useForm} from "../../hooks/useForm";
import {activeNote, startDeleting} from "../../actions/notes";

export const NoteScreen = () => {

    const {active: note} = useSelector(state => state.notes);
    const {theme} = useSelector(state => state.theme);

    const dispatch = useDispatch();
    const [values, handleInputChange, reset] = useForm(note);
    const activeId = useRef(note.id);

    useEffect(() => {

        if (note.id !== activeId.current) {

            reset(note);
            activeId.current = note.id;

        }

    }, [reset, note]);

    useEffect(() => {

        dispatch(activeNote(values.id, {...values}));

    }, [dispatch, values]);

    console.log(values);

    const {body, title} = values;

    const handleDelete = () => {
        dispatch(startDeleting(activeId.current));
    }

    return (

        <div className={`${theme === 'light' ? "notes__main-content-light" : "notes__main-content-dark"}`}>

            <NotesAppBar/>

            <div className="notes__content">

                <input type="text" placeholder="Some awesome title"
                       className={`${theme === 'light' ? "notes__title-input-light" : "notes__title-input-dark"}`}
                       value={title} name="title" onChange={handleInputChange} autoComplete="nope"/>

                <textarea placeholder="What happened today" value={body} name="body"
                          onChange={handleInputChange}
                          className={`${theme === 'light' ? "notes__textarea-light" : "notes__textarea-dark"}`}>
                </textarea>

                {(note.url) &&
                <div className="notes__image mt-3">
                    <img src={note.url} alt={title}/>
                </div>}

                <button className="btn btn-danger mt-3" onClick={handleDelete}>
                    Delete
                </button>

            </div>

        </div>

    )
};