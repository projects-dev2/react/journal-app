import React from 'react';
import {Link} from "react-router-dom";

import {useForm} from "../../hooks/useForm";

import {useDispatch, useSelector} from "react-redux";

import {startFacebookLogin, startGithubLogin, startGoogleLogin, startLoginEmailPassword} from "../../actions/auth";

export const LoginScreen = () => {

    const dispatch = useDispatch();
    const {loading} = useSelector(state => state.ui);
    console.log('loading: ',loading);

    const [values, handleInputChange] = useForm({
        email: 'testKyaa@gmail.com',
        password: '123456'
    });

    const {email, password} = values;

    const handleLogin = (e) => {
        e.preventDefault();
        if (isLoginValid()) {
            console.log('Login is correct')
            dispatch(startLoginEmailPassword(email, password));
        }
    }

    const handleGoogleLogin = () => {
        dispatch(startGoogleLogin());
    }

    const handleFacebookLogin = () => {
        dispatch(startFacebookLogin());
    }

    const handleGithubLogin = () => {
        dispatch(startGithubLogin());
    }

    const isLoginValid = () => {
        if (email.trim().length === 0) {
            console.log('email is required');
            return false;

        } else if (password.trim().length === 0) {
            console.log('password is required');
            return false;
        }

        return true;
    }

    return (

        <div className="animate__animated animate__fadeIn animate__fast">
            <h3 className="auth__title">Login</h3>
            <form onSubmit={handleLogin}>

                <input type="text"
                       placeholder="Your email"
                       name="email"
                       className="auth__input"
                       autoComplete="nope"
                       value={email}
                       onChange={handleInputChange}
                />

                <input type="text"
                       placeholder="Your password"
                       name="password"
                       className="auth__input"
                       autoComplete="nope"
                       value={password}
                       onChange={handleInputChange}
                />

                <button type="submit" disabled={loading}
                        className="btn btn-primary btn-block">
                    Login
                </button>

                <div className="auth__social-networks">
                    <p className="mb-5 mt-1 not-selection">Login with social network</p>

                    <div className="google-btn not-selection" onClick={handleGoogleLogin}>
                        <div className="google-icon-wrapper">
                            <img className="google-icon"
                                 // src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg"
                                 src="/assets/google.png"
                                 alt="google button"/>
                        </div>
                        <p className="btn-text">
                            <b>Sign in with google</b>
                        </p>
                    </div>

                    <div className="facebook-btn not-selection" onClick={handleFacebookLogin}>
                        <div className="facebook-icon-wrapper">
                            <img className="facebook-icon"
                                 src="/assets/facebook.png"
                                 alt="facebook button"/>
                        </div>
                        <p className="btn-text">
                            <b>Sign in with Facebook</b>
                        </p>
                    </div>

                    <div className="github-btn not-selection" onClick={handleGithubLogin}>
                        <div className="github-icon-wrapper">
                            <img className="github-icon"
                                 src="/assets/github.png"
                                 alt="facebook button"/>
                        </div>
                        <p className="btn-text">
                            <b>Sign in with Github</b>
                        </p>
                    </div>

                </div>

                <Link to="/auth/register"
                      className="link not-selection">
                    Create new account
                </Link>

            </form>
        </div>

    )
};