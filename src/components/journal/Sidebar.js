import React from 'react';
import {useDispatch, useSelector} from "react-redux";

import {JournalEntries} from "./JournalEntries";
import {Spinner} from "../ui/Spinner";

import {startLogout} from "../../actions/auth";
import {startNewNote} from "../../actions/notes";
import {startingChangeTheme} from "../../actions/theme";

export const Sidebar = () => {

    // console.log('A')
    const dispatch = useDispatch();
    // console.log('A')

    const {name, photo} = useSelector(state => state.auth);
    // console.log('A')
    const {notes} = useSelector(state => state.notes);
    // console.log('A')

    const {theme} = useSelector(state => state.theme);
    // console.log('A')

    console.log(notes);

    const handleLogout = () => {
        console.log('Logout');
        dispatch(startLogout());
    }

    const handleAddNewEntry = () => {
        dispatch(startNewNote());
    }

    const handleChangeTheme = () => {
        dispatch(startingChangeTheme());
    }

    return (

        <div className={`${theme === 'light' ? "journal__sidebar-light" : "journal__sidebar-dark"}`}
             id="sidebar-component">

            <aside
                className="journal__sidebar">

                <div className="journal__sidebar-navbar">

                    <i className={`fas fa-adjust ${theme === 'light' ?
                        "journal__modeTheme-light" :
                        " fa-rotate-180 journal__modeTheme-dark"}`}
                       onClick={handleChangeTheme}/>


                    <img className="journal__profile-photo" src={photo} alt={`Profile of ${name}`}/>

                    <h3 className="journal__profile-name">
                        {!name && <span> Name User </span>}
                        {name.split(' ').length === 1 && <span> {name}</span>}
                        {name.split(' ').length === 2 &&
                        <span> {name.split(' ')[0] + ' ' + name.split(' ')[1]}</span>}
                        {name.split(' ').length > 2 &&
                        <span> {name.split(' ')[0] + ' ' + name.split(' ')[2]}</span>}
                    </h3>


                    <button className="btn btn-logout " onClick={handleLogout}>
                        Logout
                    </button>

                </div>

                <div className="journal__new-entry" onClick={handleAddNewEntry}>

                    <i className="far fa-calendar-plus fa-5x"/>
                    <p className="mt-5">New Entry</p>

                </div>

                {
                    !notes &&
                    (<div className="journal__spinner">
                        <Spinner/>
                    </div>)
                }

                {(notes && notes.length !== 0) && (<JournalEntries/>)}

                {
                    (notes && notes.length === 0) &&
                    <p className="mt-10 journal__empty">No hay registros en esta cuenta</p>
                }

            </aside>

        </div>

    )
};