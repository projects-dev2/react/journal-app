import React, {useState} from 'react';
import {useSelector} from "react-redux";

export const NothingSelected = () => {

    const {theme} = useSelector(state => state.theme);
    const [closeModal, setCloseModal] = useState(true);

    const handleContractTwo = () => {

        let menuDiv = document.querySelector('#sidebar-component');

        if (closeModal) {
            menuDiv.classList.add('sidebar-close');
        } else {
            if (theme === 'light') {
                menuDiv.classList.add('journal__sidebar-light');
            } else {
                menuDiv.classList.add('journal__sidebar-dark');
            }
            menuDiv.classList.remove('sidebar-close');
        }

        setCloseModal(!closeModal);
    }


    return (

        <div
            className={`nothing-container ${theme === 'light' ? "nothing-background-light" : "nothing-background-dark"}`}>

            <span className="nothing__contract" onClick={handleContractTwo}>
                <i className="fas fa-bars"/>
            </span>

            <div className="nothing_main-content">

                <div className="nothing__main-center">

                    <p>Select something <br/>or create an entry!</p>
                    <p><i className="far fa-star fa-4x "/></p>

                </div>

            </div>

        </div>

    )
};