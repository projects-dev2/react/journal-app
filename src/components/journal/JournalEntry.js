import React from 'react';

import moment from 'moment';
import 'moment/locale/es';

import {useDispatch} from "react-redux";
import {activeNote} from "../../actions/notes";

export const JournalEntry = ({id, date, title, body, url}) => {

    const dispatch = useDispatch();
    const noteDate = moment(date);

    const handleEntryClick = () => {
        dispatch(
            activeNote(id,{
                date, title, body, url
            } )
        );
    }

    return (

        <div className="journal__entry pointer" onClick={handleEntryClick}>

            {url && <div className="journal__entry-picture"
                         style={{
                             backgroundSize: 'cover',
                             backgroundImage: `url(${url})`
                         }}>
            </div>}

            <div className="journal__entry-body">
                <p className="journal__entry-title">{title}</p>

                { body.length <= 30 && <p className="journal__entry-content">{body}</p> }
                {  body.length > 30  && <p className="journal__entry-content">{body.substring(0,25) } ... </p>}

            </div>

            <div className="journal__entry-date-box">
                <span>{noteDate.format('DD/MM/GGGG')}</span>
                <h4>{noteDate.format('h:mm A')}</h4>

            </div>

        </div>

    )
};