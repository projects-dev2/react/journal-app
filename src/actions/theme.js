import {types} from "../types/types";

export const changeTheme = (modeTheme) => ({
    type: types.themeUpdated,
    payload: modeTheme
});

export const startingChangeTheme = () => {

    return (dispatch, getState) => {

        const {theme} = getState().theme;

        if (theme === 'light') {
            dispatch(changeTheme('dark'));
        } else {
            dispatch(changeTheme('light'));
        }

        //
    }
}
