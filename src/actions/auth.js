import {types} from "../types/types";

import Swal from 'sweetalert2';

import {googleAuthProvider, facebookAuthProvider, githubAuthProvider} from '../firebase/firebaseConfig';

import {
    getAuth,
    signInWithPopup,
    createUserWithEmailAndPassword,
    updateProfile,
    signInWithEmailAndPassword,
    signOut,
} from 'firebase/auth';

import {finishLoading, startLoading} from "./ui";
import {noteLogout} from "./notes";

export const startLoginEmailPassword = (email, password) => {

    return (dispatch) => {

        dispatch(startLoading());

        signInWithEmailAndPassword(getAuth(), email, password)
            .then(async ({user}) => {

                dispatch(login(user.uid, user.displayName));
                dispatch(finishLoading());

            })
            .catch((error) => {
                console.log({error})
                dispatch(finishLoading());
                Swal.fire('Error', error.message, 'error');
            });

        Swal.close();

    }
};

export const startRegisterWithEmailPasswordName = (email, password, name) => {

    return (dispatch) => {

        createUserWithEmailAndPassword(getAuth(), email, password)
            .then(async ({user}) => {

                await Swal.fire({
                    title: 'Éxito',
                    text: 'Se registro correctamente',
                    icon: 'success',
                    allowOutsideClick: false,
                })

                await updateProfile(user, {displayName: name});
                dispatch(login(user.uid, user.displayName));
                console.log(user);

            })
            .catch(error => {

                Swal.fire({
                    title: 'Error',
                    text: error.message,
                    icon: 'error',
                    allowOutsideClick: false,
                })
            })

        Swal.close();

    }
};

export const startGoogleLogin = () => {
    return (dispatch) => {

        // According to documentation official of firebase  -> const auth = getAuth();
        signInWithPopup(getAuth(), googleAuthProvider)
            .then(({user}) => {
                dispatch(login(user.uid, user.displayName, user.photoURL))
            });

    }
};

export const startFacebookLogin = () => {
    return (dispatch) => {

        signInWithPopup(getAuth(), facebookAuthProvider)
            .then((result) => {
                console.log(result);
                dispatch(login(result.user.uid, result.user.displayName, result.user.photoURL));
            })
            .catch(error => {
                console.log({error});
            });
    }
};

export const startGithubLogin = () => {
    return (dispatch) => {

        signInWithPopup(getAuth(), githubAuthProvider)
            .then((result) => {

                let name =  result.user.reloadUserInfo.providerUserInfo[0].displayName ||
                            result.user.reloadUserInfo.screenName || 'User';

                dispatch(login(result.user.uid, name, result.user.photoURL));
            })
            .catch(error => {
                console.log({error});
            });
    }
};

export const login = (uid, displayName, photo) => ({

    type: types.authLogin,
    payload: {
        uid: uid,
        displayName,
        photo,
    }

});

export const startLogout = () => {

    return async (dispatch) => {
        await signOut(getAuth());
        dispatch(logout());
        dispatch(noteLogout())
    }

}

export const logout = () => ({
    type: types.authLogout
});

