import Swal from 'sweetalert2';

import {db} from "../firebase/firebaseConfig";
import {collection, doc, addDoc, updateDoc, deleteDoc} from "firebase/firestore";

import {types} from "../types/types";

import {loadNotes} from "../helpers/loadNotes";
import {fileUpload} from "../helpers/fileUpload";

// function async ----> ()=> { return }
export const startNewNote = () => {

    return async (dispatch, getState) => {

        const {uid} = getState().auth;

        const newNote = {
            title: '',
            body: '',
            date: new Date().getTime()
        }

        const documento = await addDoc(collection(db, `${uid}/journal/notes`), newNote)

        dispatch(activeNote(documento.id, newNote));

        Swal.fire({
            title: 'Éxito',
            text: 'Se guardó correctamente',
            icon: 'success',
            allowOutsideClick: false,
        })

        dispatch( addNewNote( documento.id, newNote ) );

    }

}

// function sync ----> ()=> ({})
export const activeNote = (id, note) => ({

    type: types.notesActive,
    payload: {
        id,
        ...note
    }

})

export const addNewNote = ( id, note ) => ({
    type: types.notesAddNew,
    payload: {
        id,
        ...note
    }
})

export const startLoadingNotes = (uid) => {

    return async (dispatch) => {

        const notesPartial = null;
        dispatch(setNotes(notesPartial))
        // console.log({notesPartial})

        const notes = await loadNotes(uid);
        dispatch(setNotes(notes))
        console.log({notes})
    }

}

export const setNotes = (notes) => ({
    type: types.notesLoad,
    payload: notes
})

export const startSaveNote = (note) => {
    return async (dispatch, getState) => {

        const {uid} = getState().auth;

        if (!note.url) {
            delete note.url;
        }

        const noteToFirestore = {...note};
        // delete noteToFirestore.id;

        const uriRef = doc(db, `${uid}/journal/notes/${note.id}`);
        await updateDoc(uriRef, noteToFirestore);

        console.log("YA");

        Swal.fire({
            title: 'Éxito',
            text: 'Se guardó correctamente',
            icon: 'success',
            allowOutsideClick: false,
        })

        // dispatch(startLoadingNotes(uid));
        dispatch(refreshNote(note.id, noteToFirestore));

    }
}

export const refreshNote = (id, note) => ({
    type: types.notesUpdated,
    payload: {
        id,
        note
        // note: {
        //     id,
        //     ...note
        // }
    }
});

export const startUploading = (file) => {
    return async(dispatch, getState) => {

        const {active:activatedNote} = getState().notes;

        Swal.fire({
            title: 'Uploading...',
            text: 'Please wait',
            didOpen: () => {
                Swal.showLoading();
            },
            allowOutsideClick: false,
            // icon: 'success',
        })

        activatedNote.url = await fileUpload(file);
        dispatch(startSaveNote(activatedNote));

        Swal.close();

    }
}

export const startDeleting = (id) =>{

    return async(dispatch, getState)=>{

        const {uid} = getState().auth;
        console.log({id})

        const resp = await deleteDoc(doc(db, `${uid}/journal/notes/${id}`));
        console.log({resp})

        Swal.fire({
            title: 'Removed',
            text: 'Was successfully removed',
            allowOutsideClick: false,
            icon: 'success',
        })

        dispatch(deleteNote(id));

    }

}

export const deleteNote = (id)=>({
    type : types.notesDelete,
    payload: id
})

export const noteLogout = ()=>({

    type: types.notesLogoutCleaning

})