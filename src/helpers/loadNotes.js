import {db} from "../firebase/firebaseConfig";
import {collection, getDocs} from "firebase/firestore";

export const loadNotes = async (uid) => {

    const notesSnap = await getDocs(collection(db, `${uid}/journal/notes`));

    /**
     * console.log(notesSnap);
     *
     * const filterData = query(collection(db, `${uid}/journal/notes`), orderBy('asc'));
     * console.log(filterData);
     */

    const notes = [];

    notesSnap.forEach((snapHijo) => {
        notes.push({
            id: snapHijo.id,
            ...snapHijo.data()
        });
    });

    // console.log({notes})

    return notes;

}